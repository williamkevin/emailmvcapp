package com.example.emailmvcapp.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.example.emailmvcapp.R
import com.example.emailmvcapp.model.post.PostLogin
import com.example.emailmvcapp.model.response.ResponseLogin
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        edt_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    edt_email.setError(getString(R.string.error_message_empty))

                } else if (isEmailValid(s.toString().trim())) {
                    edt_email.setError(null)

                } else {
                    edt_email.setError(getString(R.string.error_invalid_email))
                }
            }

        })

        edt_password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    edt_password.setError(getString(R.string.error_message_empty))

                } else {
                    edt_password.setError(null)

                }
            }

        })

        btn_login.setOnClickListener {

            if (edt_email.text.toString().isEmpty() || edt_password.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.error_message_empty), Toast.LENGTH_SHORT)
                    .show()
            } else if (!isEmailValid(edt_email.text.toString())) {
                Toast.makeText(this, getString(R.string.toast_valid_email), Toast.LENGTH_SHORT)
                    .show()
            } else {
                val loginUser = APIClient().getClient()?.create(APIInterface::class.java)
                    ?.postLoginUser(
                        PostLogin(
                            edt_email.text.toString(),
                            edt_password.text.toString()
                        )
                    )
                loginUser?.enqueue(object : Callback<ResponseLogin> {
                    override fun onFailure(call: Call<ResponseLogin>, t: Throwable) {
                        Toast.makeText(
                            applicationContext,
                            getString(R.string.toast_on_failure),
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    override fun onResponse(
                        call: Call<ResponseLogin>,
                        response: Response<ResponseLogin>
                    ) {
                        if (response.isSuccessful) {
                            Toast.makeText(
                                applicationContext,
                                getString(R.string.toast_is_successful),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            Toast.makeText(
                                applicationContext,
                                getString(R.string.toast_is_failed),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                })

            }
        }
    }

    fun isEmailValid(email: String): Boolean {
        val regExpn = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")
        val inputStr: CharSequence = email
        val pattern =
            Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(inputStr)
        return matcher.matches()
    }

}

