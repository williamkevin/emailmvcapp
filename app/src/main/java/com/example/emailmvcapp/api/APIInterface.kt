package com.g2projectteam2.mocap.api

import com.example.emailmvcapp.model.post.PostLogin
import com.example.emailmvcapp.model.response.ResponseLogin
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface APIInterface {

    @POST("signin")
    fun postLoginUser(@Body postLogin: PostLogin): Call<ResponseLogin>
}
