package com.example.emailmvcapp.model.response

data class ResponseLogin(
    val token_type: String,
    val access_token: String
)
